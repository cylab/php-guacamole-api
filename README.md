# php-guacamole-api

[![pipeline status](https://gitlab.cylab.be/cylab/php-guacamole-api/badges/master/pipeline.svg)](https://gitlab.cylab.be/cylab/php-guacamole-api/commits/master)
[![coverage report](https://gitlab.cylab.be/cylab/php-guacamole-api/badges/master/coverage.svg)](https://gitlab.cylab.be/cylab/php-guacamole-api/commits/master)

PHP library to configure the [Apache Guacamole](https://guacamole.apache.org/) remote desktop gateway.

![](./apache_guacamole.jpeg)


## Installation and usage
```bash
composer require cylab/php-guacamole-api
```


```php
require_once "vendor/autoload.php";

use \Cylab\Guacamole\Guacamole;
use \Cylab\Guacamole\User;
use \Cylab\Guacamole\Connection;

Guacamole::connect($host, $username, $password);

$user = new User();
$user->setUsername("testuser");
$user->setPassword("passw0rd");
$user->save();

$connection = new Connection;
$connection->setName("Linux Desktop");
$connection->setProtocol(Connection::RDP);
$connection->setHost("127.0.0.1");
$connection->setPort(3389);
$connection->save();

$user->addConnection($connection);

foreach (User::all() as $u) {
  echo $u->getId() . " : " . $u->getUsername() . "\n";
}

foreach ($user->connections as $c) {
  echo $c->getId . "\n";
}

```

## Contributing and testing

To run the tests, php-guacamole-api expects a **running guacamole instance**. 

To run the unit tests, you can use the provided **docker-compose.yaml** to run a MySQL container that will simulate guacamole:

```bash
cp env.docker .env
docker-compose up
./vendor/bin/phpunit
```