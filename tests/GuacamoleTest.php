<?php

namespace Cylab\Guacamole;

use Carbon\Carbon;

/**
 * @group guacamole
 */
class GuacamoleTest extends \PHPUnit\Framework\TestCase
{


    private $host = "";
    private $username = "";
    private $password = "";

    protected function setUp(): void
    {
        $dotenv = \Dotenv\Dotenv::create(__DIR__ . "/../");
        $dotenv->load();

        $this->host = getenv("MYSQL_HOST");
        $this->username = getenv("MYSQL_USER");
        $this->password = getenv("MYSQL_PASSWORD");

        $dsn = 'mysql:host=' . $this->host . ';port=3306;';
        $pdo = new \PDO($dsn, $this->username, $this->password);

        $pdo->exec("DROP DATABASE guacamole;");
        $pdo->exec("CREATE DATABASE guacamole;");
        $pdo->exec("USE guacamole;");

        $sql = file_get_contents(__DIR__ . '/001-create-schema.sql');
        $pdo->exec($sql);

        Guacamole::connect($this->host, $this->username, $this->password);
    }

    public function testUserRecord()
    {
        $r = new UserRecord();
        $r->username = "test";
        $r->start_date = Carbon::now();
        $r->save();

        $r = new UserRecord();
        $r->username = "test";
        $r->start_date = Carbon::now()->subDays(2);
        $r->save();

        $this->assertEquals(1, UserRecord::countActiveConnections());
    }

    public function testConnectionRecord()
    {
        $user = User::create("testuser", "passw0rd");

        $r = new ConnectionRecord();
        $r->user()->associate($user);
        $r->username = "test";
        $r->connection_name = "connection";
        $r->start_date = Carbon::now();
        $r->save();

        $this->assertEquals("testuser", $r->user->getUsername());
        $this->assertEquals(1, $user->connectionRecords()->count());

        $conn = new Connection;
        $conn->setName("test connection");
        $conn->setProtocol(Connection::RDP);
        $conn->save();

        $r->connection()->associate($conn);
        $r->save();

        $this->assertEquals(1, $conn->records()->count());
    }

    public function testConnection()
    {
        $conn = new Connection;
        $conn->setName("test connection");
        $conn->setHost("1.2.3.4");
        $conn->setPort(1234);
        $conn->setProtocol(Connection::RDP);
        $conn->save();

        $this->assertNotNull($conn->getId());

        $connections = Connection::all();
        $this->assertEquals(1, count($connections));

        $indb = $connections[0];
        $this->assertEquals("rdp", $indb->getProtocol());
        $this->assertEquals(1234, $indb->getPort());
        $this->assertEquals("1.2.3.4", $indb->getHost());
        $this->assertEquals("1.2.3.4", $conn->getParameter("hostname"));

        // check that we can change the connection settings...
        $indb->setHost("127.0.0.1");

        $connections = Connection::byPort(1234);
        $this->assertEquals(1, count($connections));
        $this->assertEquals("test connection", $connections[0]->getName());

        $this->assertEquals(0, count(Connection::byPort(155)));
    }

    public function testUser()
    {

        $user = User::create("testuser", "passw0rd");
        $this->assertNotNull($user->getId());

        $user->setEmailAddress("foo@example.com");
        $user->setUsername("new name");
        $user->save();

        $user_id = $user->getId();

        $user2 = User::where("user_id", $user_id)->first();
        $this->assertEquals("foo@example.com", $user2->getEmailAddress());
        $this->assertEquals("new name", $user2->getUsername());

        $this->assertEquals("new name", User::byUsername("new name")->getUsername());
        $this->assertEquals(null, User::byUsername("nobody"));

        $connection = new Connection;
        $connection->setName("test connection");
        $connection->setProtocol(Connection::RDP);
        $connection->save();

        $user->addConnection($connection);

        $this->assertEquals(1, $connection->users()->count());
        $this->assertEquals(1, $user->connections()->count());

        $user->delete();
        $this->assertEquals(0, User::count());
        // check that the corresponding entity was correctly deleted
        $this->assertEquals(0, Entity::count());

        $user = User::create("new name", "passw0rd");
        $this->assertEquals(1, User::count());
    }
}
