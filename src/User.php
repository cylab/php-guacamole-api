<?php

namespace Cylab\Guacamole;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of User
 *
 * @property int $user_id
 * @property Entity $entity
 * @property string $password_salt
 * @property string $password_hash
 * @property string $password_date
 * @property string $email_address
 * @property type $connections
 * @mixin \Eloquent
 *
 * @author tibo
 */
class User extends Model
{

    const DATE_FORMAT = "Y-m-d H:i:s";

    protected $connection = 'guacamole';
    protected $table = "guacamole_user";
    protected $primaryKey = "user_id";
    public $timestamps = false;

    public static function create(string $username, string $password) : User
    {
        $entity = new Entity();
        $entity->setType("USER");
        $entity->setName($username);
        $entity->save();

        $user = new User();
        $user->entity()->associate($entity);
        $user->setPassword($password);
        $user->save();

        return $user;
    }

    public static function byUsername(string $username) : ?User
    {
        $entity = Entity::where("name", $username)->first();
        return $entity->user ?? null;
    }

    public function entity()
    {
        return $this->belongsTo(Entity::class, 'entity_id');
    }

    public function setUsername(string $username)
    {
        $this->entity->setName($username);
        $this->entity->save();
    }

    /**
     * Set password (clear-text)
     * @param String $password clear-text password
     */
    public function setPassword($password)
    {
        $salt = random_bytes(32);
        $this->password_salt = $salt;
        $this->password_hash = $this->hash($salt, $password);
        $this->password_date = date(self::DATE_FORMAT);
    }

    /**
     * Set email address of user
     */
    public function setEmailAddress(string $email)
    {
        $this->email_address = $email;
    }

    /**
     *
     * @param string $salt
     * @param string $password
     * @return string binary hashed password
     */
    public function hash($salt, $password)
    {
        $concat = $password . strtoupper(bin2hex($salt));
        return hash(
            "sha256",
            $concat,
            true
        );     // return raw binary (instead of hexadecimal)
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function getUsername()
    {
        return $this->entity->getName();
    }

    public function getEmailAddress() : string
    {
        return $this->email_address;
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function connections()
    {
        return $this->entity->connections();
    }

    public function addConnection(Connection $connection)
    {
        $this->entity->addConnection($connection);
    }

    public function connectionRecords()
    {
        return $this->hasMany(ConnectionRecord::class, 'user_id', 'user_id');
    }

    public function delete()
    {
        $this->entity->delete();
        return parent::delete();
    }
}
