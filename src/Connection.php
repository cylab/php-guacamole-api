<?php

namespace Cylab\Guacamole;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Connection
 *
 * @property int $connection_id
 * @property string $connection_name
 * @property string $protocol rdp or vnc
 *
 * @author tibo
 */
class Connection extends Model
{

    protected $connection = 'guacamole';
    protected $table = "guacamole_connection";
    protected $primaryKey = "connection_id";
    public $timestamps = false;

    const RDP = "rdp";
    const VNC = "vnc";

    public function __construct()
    {
        parent::__construct();
        $this->setProtocol(self::RDP);
    }

    public function getId() : int
    {
        return $this->connection_id;
    }

    public function getName() : string
    {
        return $this->connection_name;
    }

    public function setName(string $name)
    {
        $this->connection_name = $name;
    }

    public function getProtocol() : string
    {
        return $this->protocol;
    }

    public function setProtocol(string $protocol)
    {
        $this->protocol = $protocol;
    }

    public function parameters()
    {
        return $this->hasMany(
            "Cylab\Guacamole\ConnectionParameter",
            "connection_id",
            "connection_id"
        );
    }

    public function getParameter(string $name)
    {
        return $this->parameters()
                ->where("parameter_name", $name)
                ->first()
                ->getValue();
    }

    public function setParameter(string $name, $value)
    {
        if (!$this->exists) {
            // so we have an id
            $this->save();
        }

        $param = $this->parameters()->where("parameter_name", $name)->first();

        if ($param !== null) {
            // there is already a paramter with this name for this connection...
            // the connection_parameter table uses a compound primary key
            // while eloquent does not support compound primary keys
            // which causes an error when you try to update a row
            // the quick fix is to delete the old row, and create a new one...
            ConnectionParameter::where([
                "parameter_name" => $name,
                "connection_id" => $this->connection_id])->delete();
        }

        $param = new ConnectionParameter($name, $value);
        $param->connection()->associate($this);
        $param->save();
    }

    public function getHost() : string
    {
        return $this->getParameter("hostname");
    }

    public function getPort() : int
    {
        return (int) $this->getParameter("port");
    }

    public function setHost(string $host)
    {
        $this->setParameter("hostname", $host);
    }

    public function setPort(int $port)
    {
        $this->setParameter("port", $port);
    }

    public function entities()
    {
        return $this->belongsToMany(
            'Cylab\Guacamole\Entity',
            'guacamole_connection_permission',
            'connection_id',
            'entity_id'
        )
                ->using('Cylab\Guacamole\ConnectionPermission');
    }

    public function users()
    {
        return $this->entities()->where("type", "USER");
    }

    public function records()
    {
        return $this->hasMany(
            ConnectionRecord::class,
            'connection_id',
            'connection_id'
        );
    }

    /**
     *
     * @param int $port
     * @return array<Connection>
     */
    public static function byPort(int $port) : array
    {
        $connections = [];
        foreach (ConnectionParameter::where("parameter_name", "port")
                ->where("parameter_value", $port)
                ->get() as $parameter) {

            /** @var ConnectionParameter $parameter */
            $connections[] = $parameter->connection()->first();
        }

        return $connections;
    }
}
