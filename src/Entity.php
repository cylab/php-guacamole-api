<?php

namespace Cylab\Guacamole;

/**
 * Base class for a User or a UserGroup
 *
 * @property string $name
 * @property string $type USER or USER_GROUP
 * @mixin \Eloquent
 *
 * @author tibo
 */
class Entity extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'guacamole';
    protected $table = "guacamole_entity";
    protected $primaryKey = "entity_id";
    public $timestamps = false;

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function connections()
    {
        return $this->belongsToMany(
            'Cylab\Guacamole\Connection',
            'guacamole_connection_permission',
            'entity_id',
            'connection_id'
        )
                ->using('Cylab\Guacamole\ConnectionPermission');
    }

    public function user()
    {
        if ($this->type !== "USER") {
            throw new \Exception("This entity is NOT a user : " . $this->type);
        }

        return $this->hasOne(User::class, 'entity_id');
    }

    public function addConnection(Connection $connection)
    {
        $this->connections()->save($connection, ["permission" => "READ"]);
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * USER or USER_GROUP
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    const TYPES = ["USER", "USER_GROUP"];

    public function setType(string $type)
    {
        if (!in_array($type, self::TYPES)) {
            throw new \Exception("Invalid type: $type");
        }

        $this->type = $type;
    }
}
