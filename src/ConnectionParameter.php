<?php

namespace Cylab\Guacamole;

/**
 * Description of ConnectionParameter
 *
 * @property string $parameter_name
 * @property string $parameter_value
 * @mixin \Eloquent
 * @author tibo
 */
class ConnectionParameter extends \Illuminate\Database\Eloquent\Model
{

    protected $connection = 'guacamole';
    protected $table = "guacamole_connection_parameter";
    public $timestamps = false;

    public function __construct($name = "", $value = "")
    {
        $this->parameter_name = $name;
        $this->parameter_value = $value;
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function connection()
    {
        return $this->belongsTo(Connection::class, "connection_id", "connection_id");
    }

    public function getName()
    {
        return $this->parameter_name;
    }

    public function getValue()
    {
        return $this->parameter_value;
    }
}
