<?php

namespace Cylab\Guacamole;

use Carbon\Carbon;

/**
 * Represents an actual connection from a user
 *
 * @property string $username
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property string $remote_host
 * @property string $connection_name
 * @property User $user
 * @mixin \Eloquent
 */
class ConnectionRecord extends \Illuminate\Database\Eloquent\Model
{

    protected $connection = 'guacamole';
    protected $table = "guacamole_connection_history";
    protected $primaryKey = "history_id";
    public $timestamps = false;

    protected $dates = ['start_date', 'end_date'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function connection()
    {
        return $this->belongsTo(Connection::class, 'connection_id');
    }

    /**
     *
     * @return Carbon
     */
    public function getStartDate() : Carbon
    {
        return $this->start_date;
    }

    /**
     * Get the time the connection was closed (or null if the connection is
     * still active)
     * @return Carbon|null
     */
    public function getEndDate() : ?Carbon
    {
        return $this->end_date;
    }

    /**
     * Get the IP from which the user is connected.
     * @return string|null
     */
    public function getRemoteHost() : ?string
    {
        return $this->remote_host;
    }
}
