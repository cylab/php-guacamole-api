<?php

namespace Cylab\Guacamole;

use Carbon\Carbon;

/**
 * Represents a web connection from a user.
 *
 * @property string $username
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property string $remote_host
 * @mixin \Eloquent
 */
class UserRecord extends \Illuminate\Database\Eloquent\Model
{

    protected $connection = 'guacamole';
    protected $table = "guacamole_user_history";
    protected $primaryKey = "history_id";
    public $timestamps = false;

    protected $dates = ['start_date', 'end_date'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     *
     * @return Carbon
     */
    public function getStartDate() : Carbon
    {
        return $this->start_date;
    }

    /**
     * Get the time the connection was closed (or null if the connection is
     * still active)
     * @return Carbon|null
     */
    public function getEndDate() : ?Carbon
    {
        return $this->end_date;
    }

    /**
     * Get the IP from which the user is connected.
     * @return string|null
     */
    public function getRemoteHost() : ?string
    {
        return $this->remote_host;
    }

    /**
     * Count the number active web connections.
     */
    public static function countActiveConnections()
    {
        $start = Carbon::today()->subDay()->toDateTimeString();

        return self::where("end_date", null)
                ->where("start_date", ">=", $start)
                ->count();
    }
}
