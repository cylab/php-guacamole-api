<?php
namespace Cylab\Guacamole;

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Description of Guacamole
 *
 * @author tibo
 */
class Guacamole
{


    public static function connect(
        $host,
        $username,
        $password,
        $db = "guacamole"
    ) {

        $capsule = new Capsule;
        $capsule->addConnection(
            [
            'driver' => 'mysql',
            'host' => $host,
            'database' => $db,
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''],
            "guacamole"
        );
        
        $capsule->bootEloquent();
        $capsule->setAsGlobal();
    }
}
