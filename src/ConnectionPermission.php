<?php

namespace Cylab\Guacamole;

/**
 * Description of ConnectionPermission
 *
 * @author tibo
 */
class ConnectionPermission extends \Illuminate\Database\Eloquent\Relations\Pivot
{

    protected $connection = 'guacamole';
    protected $table = "guacamole_connection_permission";
    public $timestamps = false;
}
